//
//  SchoolsView.swift
//  20220801-EduardoLopez-NYCSchools
//
//  Created by Eduardo Lopez on 8/3/22.
//

import SwiftUI

struct SchoolsView: View {
    @State var schools: [School] = []
    @State var isLoading = true
    private let requestManager = RequestManager()
    
    var body: some View {
        NavigationView {
            List {
                ForEach(schools) { school in
                    NavigationLink(destination: SchoolDetailsView(school: school)) {
                        SchoolRow(school: school)
                    }
                }
            }
            .listStyle(.plain)
            .navigationTitle("NYC Schools")
            .overlay {
                if isLoading {
                    ProgressView("Loading schools..")
                }
            }
            .task {
                await fetchSchools()
            }
        }
    }
    
    func fetchSchools() async {
        do {
            let schools: [School] = try await requestManager.perform(SchoolsRequest.getSchools(1))
            self.schools = schools
            await stopLoading()
        } catch {
            print(error)
        }
    }
    
    @MainActor
    func stopLoading() async {
        isLoading = false
    }
}

struct SchoolsView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolsView()
    }
}
