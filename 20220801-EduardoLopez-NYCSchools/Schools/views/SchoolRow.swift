//
//  SchoolRow.swift
//  20220801-EduardoLopez-NYCSchools
//
//  Created by Eduardo Lopez on 8/4/22.
//

import SwiftUI

struct SchoolRow: View {
    var school: School
    
    var body: some View {
        HStack {
            Text(school.schoolName)
        }
    }
}

struct SchoolRow_Previews: PreviewProvider {
    static var previews: some View {
        SchoolRow(school: School(dbn: "XXXX",
                                 schoolName: "Test school name",
                                 numOfSatTestTakers: "0",
                                 satCriticalReadingAvgScore: "0",
                                 satMathAvgScore: "0",
                                 satWritingAvgScore: "0"
                                ))
    }
}
