//
//  SchoolDetailsView.swift
//  20220801-EduardoLopez-NYCSchools
//
//  Created by Eduardo Lopez on 8/4/22.
//

import SwiftUI

struct SchoolDetailsView: View {
    var school: School
    var body: some View {
        VStack {
            Text(school.schoolName)
                .font(.largeTitle)
                .padding()
            Text("SAT test takers: \(school.numOfSatTestTakers)")
            Text("Math average: \(school.satMathAvgScore)")
            Text("Reding average: \(school.satCriticalReadingAvgScore)")
            Text("Writing average: \(school.satWritingAvgScore)")
            Spacer()
        }
    }
}

struct SchoolDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolDetailsView(school: School(dbn: "XXXX",
                                         schoolName: "Test school name",
                                         numOfSatTestTakers: "0",
                                         satCriticalReadingAvgScore: "0",
                                         satMathAvgScore: "0",
                                         satWritingAvgScore: "0"
                                        ))
    }
}
