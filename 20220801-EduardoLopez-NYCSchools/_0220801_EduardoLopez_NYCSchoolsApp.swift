//
//  _0220801_EduardoLopez_NYCSchoolsApp.swift
//  20220801-EduardoLopez-NYCSchools
//
//  Created by Eduardo Lopez on 8/1/22.
//

import SwiftUI

@main
struct _0220801_EduardoLopez_NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            SchoolsView()
        }
    }
}
