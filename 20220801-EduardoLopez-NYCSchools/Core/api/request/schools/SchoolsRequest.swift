//
//  SchoolsRequest.swift
//  20220801-EduardoLopez-NYCSchools
//
//  Created by Eduardo Lopez on 8/4/22.
//

import Foundation

enum SchoolsRequest: RequestProtocol {
    
    /*
     We could add params for paging, sorting, etc in a production app.
     */
    
    case getSchools(_ page: Int) // Here we could support pagination.
    
    var path: String {
        "/resource/f9bf-2cp4.json"
    }
    
    var urlParams: [String : String?] {
        switch self {
        case .getSchools(_):
            return [:]
        }
    }
    
    var requestType: RequestType{
        .GET
    }
}
