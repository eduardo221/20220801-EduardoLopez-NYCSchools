//
//  RequestType.swift
//  20220801-EduardoLopez-NYCSchools
//
//  Created by Eduardo Lopez on 8/3/22.
//

import Foundation

/*
 We dont need the REST request types PUT, PATCH and DELETE but they could be easily added later.
 */
enum RequestType: String {
    case GET
    case POST
}
