//
//  NetworkError.swift
//  20220801-EduardoLopez-NYCSchools
//
//  Created by Eduardo Lopez on 8/4/22.
//

import Foundation

public enum NetworkError: LocalizedError {
    case invalidServerResponse
    case invalidURL
    public var errorDescription: String? {
        switch self {
        case .invalidServerResponse:
            return "The server returned an invalid response."
        case .invalidURL:
            return "URL string is malformed."
        }
    }
}

/*
 We can add other possible errors available on LocalizedError
 */
