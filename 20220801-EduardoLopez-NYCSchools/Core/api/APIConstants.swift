//
//  APIConstants.swift
//  20220801-EduardoLopez-NYCSchools
//
//  Created by Eduardo Lopez on 8/3/22.
//

import Foundation

enum APIConstants {
    static let host = "data.cityofnewyork.us"
    /*
     Client ID, secret, etc could be added here.
     */
}
