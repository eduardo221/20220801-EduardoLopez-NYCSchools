//
//  Schools.swift
//  20220801-EduardoLopez-NYCSchools
//
//  Created by Eduardo Lopez on 8/4/22.
//

import Foundation

/*
  This should just work for this model, maybe not ideal but this saves a lot of time.
 */
//typealias Schools = [[String: String]]

struct School: Codable {
    let dbn: String
    let schoolName: String
    let numOfSatTestTakers: String
    let satCriticalReadingAvgScore: String
    let satMathAvgScore: String
    let satWritingAvgScore: String
}

extension School: Identifiable {
    var id: String { return dbn } // Not sure what is dbn, for this challege we can use it as the id
}



