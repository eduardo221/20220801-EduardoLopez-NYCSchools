//
//  RequestManagerTests.swift
//  20220801-EduardoLopez-NYCSchoolsTests
//
//  Created by Eduardo Lopez on 8/4/22.
//

import XCTest
@testable import _0220801_EduardoLopez_NYCSchools


class RequestManagerTests: XCTestCase {
    private var requestManager: RequestManagerProtocol?
    
    override func setUp() {
        super.setUp()
        
        requestManager = RequestManager(apiManager: APIManagerMock())
    }
    
    func testRequestSchools() async throws {
        guard let schools: [School] = try await requestManager?.perform(SchoolsRequestMock.getSchools) else {
            XCTFail("Didn't get data from the request manager")
            return
        }
        
        let first = schools.first
        let last = schools.last
        
        XCTAssertEqual(first?.schoolName, "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES")
        XCTAssertEqual(first?.numOfSatTestTakers, "29")
        XCTAssertEqual(first?.satMathAvgScore, "404")
        XCTAssertEqual(first?.satCriticalReadingAvgScore, "355")
        XCTAssertEqual(first?.satWritingAvgScore, "363")
        
        XCTAssertEqual(last?.schoolName, "BARD HIGH SCHOOL EARLY COLLEGE")
        XCTAssertEqual(last?.numOfSatTestTakers, "130")
        XCTAssertEqual(last?.satMathAvgScore, "604")
        XCTAssertEqual(last?.satCriticalReadingAvgScore, "624")
        XCTAssertEqual(last?.satWritingAvgScore, "628")
    }
   
}
