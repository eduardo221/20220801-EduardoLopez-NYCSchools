//
//  APIManagerMock.swift
//  20220801-EduardoLopez-NYCSchoolsTests
//
//  Created by Eduardo Lopez on 8/4/22.
//

import XCTest
@testable import _0220801_EduardoLopez_NYCSchools

struct APIManagerMock: APIManagerProtocol {
    func perform(_ request: RequestProtocol) async throws -> Data {
        try Data(contentsOf: URL(fileURLWithPath: request.path), options: .mappedIfSafe)
    }
}
