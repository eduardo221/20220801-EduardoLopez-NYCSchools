//
//  SchoolsRequestMock.swift
//  20220801-EduardoLopez-NYCSchoolsTests
//
//  Created by Eduardo Lopez on 8/4/22.
//

import Foundation
@testable import _0220801_EduardoLopez_NYCSchools

enum SchoolsRequestMock: RequestProtocol {
    case getSchools
    
    var requestType: RequestType {
        return .GET
    }
    
    var path: String {
        guard let path = Bundle.main.path(forResource: "SchoolsMock", ofType: "json") else { return "" }
        return path
    }
}
